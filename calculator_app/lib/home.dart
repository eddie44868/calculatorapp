import 'package:calculator_app/button.dart';
import 'package:flutter/material.dart';
import 'package:math_expressions/math_expressions.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {

  var userQuestion = '';
  var userAnswer = '';
  final List<String> buttons = [
    'C',
    'DEL',
    '%',
    '/',
    '9',
    '8',
    '7',
    'x',
    '6',
    '5',
    '4',
    '-',
    '3',
    '2',
    '1',
    '+',
    '0',
    '.',
    'ANS',
    '=',
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.deepOrange[100],
      body: Column(
        children: [
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                const SizedBox(height: 50,),
                Container(
                  padding: const EdgeInsets.all(20),
                  alignment: Alignment.centerLeft,
                  child: Text(userQuestion, style: const TextStyle(fontSize: 30),)
                ),
                Container(
                  padding: const EdgeInsets.all(20),
                  alignment: Alignment.centerRight,
                  child: Text(userAnswer, style: const TextStyle(fontSize: 30))
                ),
              ],
            ),
          ),
          Expanded(
              flex: 2,
              child: GridView.builder(
                itemCount: buttons.length,
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 4), 
                  itemBuilder: (BuildContext context, int index){
                    if (index == 0) {
                      return MyButton(
                      buttonTap: (){
                        setState(() {
                          userQuestion = '';
                          userAnswer = '0.0';
                        });
                      },
                      color: Colors.green,
                      textColor: Colors.white,
                      buttonText: buttons[index]);
                    } else if(index == 1) {
                      return MyButton(
                      buttonTap: (){
                        setState(() {
                          userQuestion = userQuestion.substring(0, userQuestion.length-1);
                        });
                      },
                      color: const Color.fromARGB(255, 234, 33, 18),
                      textColor: Colors.white,
                      buttonText: buttons[index]);
                    }

                    else if(index == buttons.length-1) {
                      return MyButton(
                      buttonTap: (){
                        setState(() {
                          equalPressed();
                        });
                      },
                      color: Colors.deepOrange,
                      textColor: Colors.white,
                      buttonText: buttons[index]);
                    }                      

                    else {
                      return MyButton(
                        buttonTap: (){
                        setState(() {
                          userQuestion += buttons[index];
                        });
                      },
                      color: isOperator(buttons[index]) ? Colors.deepOrange : Colors.deepOrange[50], 
                      textColor: isOperator(buttons[index]) ? Colors.white : Colors.deepOrange, 
                      buttonText: buttons[index]);
                    }
                  })),
        ],
      ),
    );
  }

  bool isOperator(String x){
    if (x=='%' ||x=='/' ||x=='x' ||x=='-' ||x=='+' ||x=='=') {
      return true;
    }
    return false;
  }

  void equalPressed() {
    String finalQuestion = userQuestion;
    finalQuestion = finalQuestion.replaceAll('x', '*');

    Parser p = Parser();
    Expression exp = p.parse(finalQuestion);
    ContextModel cm = ContextModel();
    double eval = exp.evaluate(EvaluationType.REAL, cm);

    userAnswer = eval.toString();
  }
}
